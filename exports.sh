#!/bin/bash
{ set -e +x; } 2>/dev/null

export username=
export password=
export TF_VAR_resource_group=

az login --username $username --password $password -o none

# https://learn.microsoft.com/en-us/cli/azure/manage-azure-subscriptions-azure-cli
export TF_VAR_subscription_id=$(az account list --query "[?isDefault].id" --output tsv)
# echo $TF_VAR_subscription_id

set -ex