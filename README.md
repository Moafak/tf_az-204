# Terraform for AZ-204 labs

* The labs for **AZ-204 Developing Solutions for Microsoft Azure** can be found on [aka.ms/az204labs](aka.ms/az204labs)
* The source code & files of the labs can be found on [AZ-204-DevelopingSolutionsforMicrosoftAzure
](https://github.com/MicrosoftLearning/AZ-204-DevelopingSolutionsforMicrosoftAzure/tree/master/Allfiles/Labs)

The labs provide instructions to create Azure resources using [Azure portal](https://portal.azure.com/) and/or the [Azure cli](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli)

> _This repo contains infrastructure code for creating the labs resources using terraform._

We will [authenticate to Azure using the Azure cli](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/azure_cli).  

Add your Azure credentials to `exports.sh` before running the labs.  

## How to obtains Azure credentials?
* You may have access to [Enterprise Skills Initiative](https://esi.microsoft.com/deliverymultiday), which will provide training for AZ-204 and provide you access to lab environments & Azure credentials
* You may have access to [O'Reilly Cloud Labs](https://learning.oreilly.com/interactive/?type=cloud-scenario&type=cloud-sandbox) which provide you an Azure Sandbox that lasts 60 minutes.
* You can get an [Azure free account](https://azure.microsoft.com/en-ca/free) 
