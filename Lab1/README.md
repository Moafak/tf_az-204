[Lab 01: Build a web application on Azure platform as a service offering](https://microsoftlearning.github.io/AZ-204-DevelopingSolutionsforMicrosoftAzure/Instructions/Labs/AZ-204_lab_01.html)

[Lab 01 files](https://github.com/MicrosoftLearning/AZ-204-DevelopingSolutionsforMicrosoftAzure/tree/master/Allfiles/Labs/01/Starter)

**Summary:**
* Create a `Storage Account`
  * Create a `Container` 
    - upload images to the container
* Create a `Service Plan`
  * Create a `Web App` for the API      
    - upload API source code
    - set Application settings: `StorageConnectionString=<Storage Account connection string>`
  * Create a `Web App` for the frontend
    - upload frontend source code, 
    - set Application settings: `ApiUrl=https://<API URL>`

**Validation:**
* Navigating to the API url should return a json array of file names in the Container.  
  (note that Container name is hardcoded in the API [appsettings.json](https://github.com/MicrosoftLearning/AZ-204-DevelopingSolutionsforMicrosoftAzure/blob/master/Allfiles/Labs/01/Starter/API/appsettings.json))
* Navigating to the frontend url should provide us with a UI to view the uploaded images and allow us to upload other images. 

**Note:**
* We will import an existing resource group in terraform (O'Reilly Azure Sandbox creates a resource group and does not allow creating another one).
* The commands to download the source code and images are provided in `run.sh`, these files will be reference in terraform code.
* To create Lab 01 Azure resources with terraform:
  ```sh
  bash run.sh
  ```