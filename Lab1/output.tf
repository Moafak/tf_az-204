output "api" {
  value = "${module.webapp_api.name}.azurewebsites.net"
}

output "ui" {
  value = "${module.webapp_ui.name}.azurewebsites.net"
}