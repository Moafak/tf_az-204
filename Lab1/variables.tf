variable "subscription_id" {
  type = string
}

variable "resource_group" {
  type = string
}

variable "location" {
  type    = string
  default = "East US"
}