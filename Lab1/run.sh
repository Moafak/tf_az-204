#!/bin/bash
PS4='\033[1;34m>>>\033[0m '; set -ex

# Download files
wget --directory-prefix=files/code https://github.com/MicrosoftLearning/AZ-204-DevelopingSolutionsforMicrosoftAzure/raw/master/Allfiles/Labs/01/Starter/API/api.zip
wget --directory-prefix=files/code https://github.com/MicrosoftLearning/AZ-204-DevelopingSolutionsforMicrosoftAzure/raw/master/Allfiles/Labs/01/Starter/Web/web.zip
wget --directory-prefix=files/images https://raw.githubusercontent.com/MicrosoftLearning/AZ-204-DevelopingSolutionsforMicrosoftAzure/master/Allfiles/Labs/01/Starter/Images/sub.jpg
wget --directory-prefix=files/images https://raw.githubusercontent.com/MicrosoftLearning/AZ-204-DevelopingSolutionsforMicrosoftAzure/master/Allfiles/Labs/01/Starter/Images/veggie.jpg

source ../exports.sh

terraform init

# terraform fmt --recursive
# terraform plan

terraform apply -auto-approve

# terraform output