variable "service_plan_id" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "location" {
  type = string
}

variable "webapp" {
  type = object({
    name            = string
    settings        = map(string)
    zip_deploy_file = string
  })
}
