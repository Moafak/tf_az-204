output "default_hostname" {
  value = azurerm_windows_web_app.webapp.default_hostname
}

output "name" {
  value = azurerm_windows_web_app.webapp.name
}