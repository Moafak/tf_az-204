resource "azurerm_windows_web_app" "webapp" {
  name = var.webapp.name

  resource_group_name = var.resource_group_name
  location            = var.location

  service_plan_id = var.service_plan_id

  zip_deploy_file = var.webapp.zip_deploy_file
  app_settings    = var.webapp.settings

  site_config {
    always_on = false
    application_stack {
      current_stack  = "dotnet"
      dotnet_version = "v6.0"
    }
  }
}