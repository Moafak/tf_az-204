resource "azurerm_resource_group" "resource_group" {
  name     = var.resource_group
  location = var.location
}

resource "random_string" "random" {
  length  = 4
  lower   = true
  upper   = false
  numeric = false
  special = false
}

module "storage" {
  source               = "./modules/storage/"
  storage_account_name = "imgstor${random_string.random.result}"
  resource_group_name  = azurerm_resource_group.resource_group.name
  location             = var.location
}

resource "azurerm_service_plan" "service_plan" {
  name                = "ManagedPlan"
  location            = var.location
  resource_group_name = azurerm_resource_group.resource_group.name

  sku_name = "S1" # https://azure.microsoft.com/en-ca/pricing/details/app-service/windows/
  os_type  = "Windows"
}

module "webapp_api" {
  source              = "./modules/webapp"
  service_plan_id     = azurerm_service_plan.service_plan.id
  resource_group_name = azurerm_resource_group.resource_group.name
  location            = var.location

  webapp = {
    name = "imgapi${random_string.random.result}"
    settings = {
      "StorageConnectionString" = module.storage.connection_string
    }
    zip_deploy_file = "files/code/api.zip"
  }
}

module "webapp_ui" {
  source              = "./modules/webapp"
  service_plan_id     = azurerm_service_plan.service_plan.id
  resource_group_name = azurerm_resource_group.resource_group.name
  location            = var.location

  webapp = {
    name = "imgweb${random_string.random.result}"
    settings = {
      "ApiUrl" = "https://${module.webapp_api.default_hostname}"
    }
    zip_deploy_file = "files/code/web.zip"
  }
}
