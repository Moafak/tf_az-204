import {
  to = azurerm_resource_group.resource_group
  id = "/subscriptions/${var.subscription_id}/resourceGroups/${var.resource_group}"
}